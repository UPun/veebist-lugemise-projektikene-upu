### Veebist lugemise projektikene ###

Käesoleva projekti ülesande tingimused on samad, nagu olid meie N3 ülesandes 3.2 Rahvusvahelised sõidukid

Ainuke vahe käesoleva tööga seisneb selles, et Faili sisu tuleb sisse lugeda veebist. On ette antud veebilehed, kus asuvad andmebaasi failid aasia.txt ja lounaameerika.txt

Faili aasia.txt sisu asub minu kodulehel: http://194.126.113.212/aasia.txt

Faili lounaameerika.txt sisu aga asub siin: http://194.126.113.212/lounaameerika.txt

Käesoleva töö eesmärk on õppida ja teha oma esimesed katsetused veebistlugemise õppimiseks. Seda läheb mul tulevastes projektides tarvis.

Selleks, et suvalist veebilehte lugeda, tuleb veel tõsiselt vaeva näha, aga esimesed sammud on sellega tehtud. NB! Faili käivitamisel on nõutav internetiühenduse olemasolu! (muidu tuleb veateade)

Muid faile peale Veebist-lugemise-projektikene-UPu.py ning õigesti veebilehe sisestamise tarvis ei lähe. Seega julget proovimist! Proovige kodulehti eraldi veebibrauseri aknas ka! (näiteks käivitades veebilehitseja Google Chrome, sisestage sinna http://194.126.113.212/aasia.txt

Proovige veel pulli pärast sisestada veebileheks mitte minu kodulehte, aga näiteks see link, mis meil oli kasutusel peatüki 5.4 KEELETÖÖTLUS all:

https://courses.cs.ut.ee/LTAT.TK.001/2017_spring/uploads/Main/eesti_keele_sonavormid.txt

Veebistlugemine toimib ka sellega naeratus (küll mitte sihtotstarbepäraselt..)

Mis aga juhtub, kui sisestate lingiks näiteks www.neti.ee?

++

20. mail lisatud foorumist loetud märkustele vastuseks väikesed parandused. Versioon 2:

Veebist-lugemise-projektikene-UPu-V2.py

Versioon 2 on lisatud mõned parandused & muudatused, mis esimeses versioonid puudusid.
1) Lisatud lihtne graafiline kasutajaliides EasyGui.
Mooduli kasutamiseks tuleb programmiga samasse kausta paigutada fail easygui.py. easygui.py lisasin, palun kopeerige see samasse kausta, muidu V2 programm ei tööta. Siiski on hetkel tegu pigem meelelahutusliku lisaga. Sinna võiks pookida lingid, aga see osa on hetkel tegemata.
2) Sisestatud Piiriületajate tähed on keeratud jõuga suurteks. Caps lock on kasutajal tihti suvalises asendis ja ebameeldiv on saada vale tulemus, kui sisestasin EST asemel kogemata est ja saan seepeale vastuseks 'Tundmatu maa'.
3) Reaktsioon Markko Meriniidu märkusele:
"Paneks ka siia märkuse juurde, et võiks vähemalt kontrollida, kas failinime küsimisel sisestati midagi või mitte. See faili mitte olemas olemine ja mitte kättesaamine ja muud veateated on omaette asjad, aga päris tühja tulemuse puhul võiks ikka failinime uuesti küsida."
V2 on lisatud fiks sellele märkusele! naeratus
4) Lisatud piiriületajate faili kirjutamine. Fail asub ja tekkib samasse kausta, kus programm ja on nimega Piiriületajad.txt

++

Veebist-lugemise-projektikene-UPu-V3.py

Versioonile 3 on lisatud faili kirjutamisel veel piiriületamise kuupäev ja kellaaeg kujul 2017-05-20 19:40:51.863092

++

Veebist-lugemise-projektikene-UPu-V4.py
Versioonile 4 on lisatud internetiühenduse kontroll. Kood on leitud siit http://stackoverflow.com/questions/3764291/checking-network-connection

21. mail 2017
UPu

P.S. veebilehe sisestamise asemel võid julgelt proovida ka lihtsalt Enterit vajutada. Olen sellega arvestanud (idee sain foorumist)- vaata, mis siis juhtub! naeratus
