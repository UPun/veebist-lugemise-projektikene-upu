# Veebist lugemise projektikene
# Ülesande tingimused on samad, nagu olid meie N3 ülesandes 3.2-Rahvusvahelised-s6idukid.py
# Ainuke vahe käesolevas töös on sellega, et Faili sisu tuleb sisse lugeda mitte failist, aga veebist. On ette antud veebilehed, kus asuvad andmebaasi failid aasia.txt ja lounaameerika.txt:

# Faili aasia.txt sisu asub minu kodulehel http://194.126.113.212/aasia.txt
# Faili lounaameerika.txt sisu aga asub siin: http://194.126.113.212/lounaameerika.txt

# Käesoleva töö eesmärk on õppida ja teha oma esimesed katsetused veebist lugemise õppimiseks. Seda läheb mul tulevastes projektides tarvis.
# Tänan kaasõppurit Ülo Müüriseppa, samuti Jüri Kormikut ning Üllar Tali- nemad võtsid sõna N3 foorumi teemas "Veebilehitseja, mis oleks kirjutatud Pythonis".


def loe_andmed_veebist():
    from urllib.request import urlopen
    #######################################################################
    print("Kopeeri või sisesta omale sobiv link andmebaasi faili nimega: ")
    print("http://194.126.113.212/aasia.txt")
    print("http://194.126.113.212/lounaameerika.txt")
    #######################################################################
    print()
    veebileht = input("Sisesta link, kus asuvad maad: ") # Sisesta palun minu kodulehe aadress:
    if veebileht == "":
        print("Peaksid siiki URL internetiaadressi lingi sisestama!")
        veebileht = input("Sisesta link, kus asuvad maad: ")
        if veebileht == "":
            print("Oled vist jonnakas eestlane? Tore on, Sinule on ette valmistatud spetsiaalne sõnaraamat, kus leiduva nii Eestis, Lõuna-Ameerikas, kui ka Aasias ringi vuravate sõidukite tunnusmärgid.")
            veebileht = "http://194.126.113.212/k6ik_koos.txt"
            print(veebileht)
    lingiNimi = veebileht
    print("Andmed lingilt: " + lingiNimi + ":")
    fail=urlopen(lingiNimi)
    read = fail.readlines()
    puhtad_read = [] # Juhuks, kui andmebaasi failis on kogemata tühjad read, või ülearused tühikud lisatud, siis nende kõrvaldamiseks loome puhtad_read järjendi.
    for rida in read:
        if rida.strip() != "":
            tekst=rida.decode()
            puhtad_read.append(tekst)
    #print(puhtad_read)
    sonastik = {}
    for rida in puhtad_read:
        jupid = rida.split(" ")
        piiriyletajad = jupid[0].strip()
        maa = jupid[1].strip()
        sonastik[piiriyletajad] = maa
    fail.close()
    #print(sonastik)
    return sonastik

def tahised_nimedeks(järjend_riikide_tähistest,sonastik):
    järjend_vastavate_riikide_nimedest=[]
    HULK_MIS_SISALDAB_TUNTUD_RIIKIDE_NIMESID = set()
    for voti, vaartus in sonastik.items():
        HULK_MIS_SISALDAB_TUNTUD_RIIKIDE_NIMESID.add(voti)
    for el in järjend_riikide_tähistest:
        elHulgaks = set([el])
        if elHulgaks & HULK_MIS_SISALDAB_TUNTUD_RIIKIDE_NIMESID:
            järjend_vastavate_riikide_nimedest.append(sonastik[el])
        else:
            järjend_vastavate_riikide_nimedest.append(None)
    return järjend_vastavate_riikide_nimedest  # Kui mõni tähis argumendiks antud järjendis on tundmatu, siis selle riigi nimi tuleb asendada tagastatavas järjendis väärtusega None.

def have_internet():
    conn = httplib.HTTPConnection("www.google.com", timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False

from easygui import *
from datetime import datetime
kuupäev_kellaeg = datetime.today()

# Internetiühenduse kontroll
try:
    import httplib
except:
    import http.client as httplib
print("Kõigepealt kontrollime internetiühenduse olemasolu. Kui see puudub, siis programm lõpetab viisakalt töö!")
print(have_internet())
if have_internet() == True:
    print("Internetiühendus OK")
else:
    print("Puudub internetiühendus, programm lõpetab töö.")
    import sys
    from sys import exit
    exit(0) # Successful exit

sisestuseTyybiValik = str(input("Kui soovid lingi sisestada EasyGuiga, sisesta lihtsalt kaks tähte EG: ")).upper()
if sisestuseTyybiValik == "EG":
    msgbox("Graafiline kasutajaliides!!")
    variandid = ["http://194.126.113.212/lounaameerika.txt","https://courses.cs.ut.ee/LTAT.TK.001/2017_spring/uploads/Main/eesti_keele_sonavormid.txt","http://194.126.113.212/aasia.txt","http://194.126.113.212/k6ik_koos.txt"]
    vajutati = choicebox("Vali link, kus asuvad maad: ", choices = variandid)
    if vajutati == None:
         msgbox("Sa ei valinud midagi!")
    else:
        msgbox(vajutati)

andmebaasiFailiNimi = loe_andmed_veebist()
print(andmebaasiFailiNimi)
Piiriületajad = str(input("Piiriületajad: ")).upper()   # J CHN EST J
PiiriületajadJärjendiks = Piiriületajad.split()
tahisedNimedeks = tahised_nimedeks(PiiriületajadJärjendiks, andmebaasiFailiNimi)

f = open("Piiriületajad.txt", "a", encoding="UTF-8")
f.write(str(kuupäev_kellaeg) + "\n")

for el in tahisedNimedeks:
    if el == None:
        print("Tundmatu maa")
        f.write(str("Tundmatu maa") + "\n")
    else:
        print(el)
        f.write(el)
        f.write("\n")
f.close()
#loe_andmed_veebist()
